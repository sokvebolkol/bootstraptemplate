<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Title Page-->
    <title>Register information</title>
    <!-- Icons font CSS-->
    <link href="mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

    <!-- CSS-->
    <link href="select2/select2.min.css" rel="stylesheet" media="all">
    <link href="datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
        <div class="wrapper wrapper--w680">
                    <div class="card card-1">
                        <div class="card-heading"></div>
                        <div class="card-body">
                            <h2 class="title">{{ __('Registration Information') }}</h2>
                            <form method="POST" action="{{route('register')}}">
                                    {{ csrf_field() }}
                                    <!-- Name && gender -->
                                    <div class="row row-space">
                                        <div class="col-2">
                                            <div class="input-group">
                                                <input class="input--style-1  @error('name') is-invalid @enderror" type="text" placeholder="NAME" id="name" name="name"  value="{{ old('name') }}" required autocomplete="name" autofocus>                                    
                                                @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="input-group">
                                            <div class="rs-select2 js-select-simple select--no-search">
                                                <select name="gender" id="gender" required>
                                                    <option value="#" disabled="disabled" selected="selected">GENDER</option>
                                                    <option value="0">Male</option>
                                                    <option value="1">Female</option>
                                                    <option value="#">Other</option>
                                                </select>
                                                <div class="select-dropdown"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Birth && address -->
                                <div class="row row-space">
                                    <div class="col-2">
                                        <div class="input-group">
                                            <input class="input--style-1 js-datepicker" type="text" placeholder="BIRTHDATE" name="birthdate" id="birthdate" required>
                                            <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="input-group">
                                            <input class="input--style-1" type="text" placeholder="ADDRESS" id="address" name="address" required>                                   
                                        </div>
                                    </div>
                                </div>
                                <!-- Class && major -->
                                <div class="row row-space">
                                    <div class="col-2">
                                        <div class="input-group">
                                        <input class="input--style-1" type="text" placeholder="GENERATION (2018-2019)" id="generation" name="generation"required>                                    
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="input-group">
                                            <div class="rs-select2 js-select-simple select--no-search">
                                                <select name="major" id="major" required>
                                                    <option value="#" disabled="disabled" selected="selected">MAJOR</option>
                                                    <option value="WEB">WEB</option>
                                                    <option value="SNA">SNA</option>
                                                    <option value="DESIGNER">DESIGNER</option>
                                                </select>
                                                <div class="select-dropdown"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Email and password -->
                                <div class="row row-space">
                                    <div class="col-2">
                                        <div class="input-group">
                                            <input id="email" class="input--style-1 @error('email') is-invalid @enderror" type="email" placeholder="EMAIL" name="email" value="{{ old('email') }}" required autocomplete="email" >                                    
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="input-group">
                                            <input class="input--style-1r" type="password" placeholder="PASSWORD" id="password" name="password" required>  
                                        </div>                                  
                                    </div>
                                </div>
                                <!-- register plan -->
                                    <div class="input-group">
                                        <div class="rs-select2 js-select-simple select--no-search">
                                            <select name="plan" id="plan" required>
                                                <option value="#" disabled="disabled" selected="selected">REGISTRATION PLAN</option>
                                                <option value="Plan 1" data-toggle="tooltip" data-placement="right" 
                                                title=" 
                                                You be able to pay for 36 months!! 
                                                -5$ per month for the first year
                                                -10$ per month for socond year
                                                -15$ for third year">Plan 1</option>
                                                <option value="Plan 2" data-toggle="tooltip" data-placement="right" 
                                                title=" 
                                                You be able to pay for 6 times!! A mount 60$ in a time">Plan 2</option>
                                                <option value="Plan 3" data-toggle="tooltip" data-placement="right" 
                                                title=" 
                                                You be able to pay for 3 tiems!! A mount 120$ in a time">Plan 3</option>
                                                <option value="Plan 4" data-toggle="tooltip" data-placement="right" 
                                                title=" You be able to pay only a time for 360$ in one time">Plan 4</option>
                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                <div class="p-t-20">
                                    <button class="btn btn--radius btn--green" type="submit"> {{ __('Register') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    <!-- Jquery JS-->
    <script src="jquery/jquery.min.js"></script>
    <!-- JS-->
    <script src="select2/select2.min.js"></script>
    <script src="datepicker/moment.min.js"></script>
    <script src="datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>

</body>

</html>
