<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// resource controller
Route::resource('user','UserController');

// protect url for user
Route::get('user','userController@index')->middleware('authenticated');


// -------------------------------Route file ----------------------------------------------

Route::get('charts',function(){
    return view('admin.charts');
});
Route::get('tables-basic',function(){
    return view('admin.tables-basic');
});
Route::get('tables-datatable',function(){
    return view('admin.tables-datatable');
});
Route::get('ui-alerts',function(){
    return view('admin.ui-alerts');
});
Route::get('ui-buttons',function(){
    return view('admin.ui-buttons');
});
Route::get('ui-cards',function(){
    return view('admin.ui-cards');
});
Route::get('ui-carousel',function(){
    return view('admin.ui-carousel');
});
Route::get('ui-collapse',function(){
    return view('admin.ui-collapse');
});
Route::get('ui-icons',function(){
    return view('admin.ui-icons');
});
Route::get('ui-modals',function(){
    return view('admin.ui-modals');
});
Route::get('ui-tooltips',function(){
    return view('admin.ui-tooltips');
});
Route::get('forms-general',function(){
    return view('admin.forms-general');
});
Route::get('forms-select2',function(){
    return view('admin.forms-select2');
});
Route::get('forms-validation',function(){
    return view('admin.forms-validation');
});
Route::get('forms-text-editor',function(){
    return view('admin.forms-text-editor');
});
Route::get('forms-datetime-picker',function(){
    return view('admin.forms-datetime-picker');
});
Route::get('forms-color-picker',function(){
    return view('admin.forms-color-picker');
});
Route::get('media-fancybox',function(){
    return view('admin.media-fancybox');
});
Route::get('media-masonry',function(){
    return view('admin.media-masonry');
});
Route::get('media-lightbox',function(){
    return view('admin.media-lightbox');
});
Route::get('media-owl-carousel',function(){
    return view('admin.media-owl-carousel');
});
Route::get('media-image-magnifier',function(){
    return view('admin.media-image-magnifier');
});
// menu blug in
Route::get('star-rating',function(){
    return view('admin.star-rating');
});
Route::get('range-sliders',function(){
    return view('admin.range-sliders');
});
Route::get('tree-view',function(){
    return view('admin.tree-view');
});
Route::get('sweetalert',function(){
    return view('admin.sweetalert');
});
Route::get('calendar',function(){
    return view('admin.calendar');
});
Route::get('gmaps',function(){
    return view('admin.gmaps');
});
Route::get('counter-up',function(){
    return view('admin.counter-up');
});
Route::get('page-pricing-tables',function(){
    return view('admin.page-pricing-tables');
});
Route::get('page-coming-soon',function(){
    return view('admin.page-coming-soon');
});
Route::get('page-invoice',function(){
    return view('admin.page-invoice');
});
Route::get('page-login',function(){
    return view('admin.page-login');
});
Route::get('page-blank',function(){
    return view('admin.page-blank');
});
Route::get('pro-settings',function(){
    return view('admin.pro-settings');
});
Route::get('pro-profile',function(){
    return view('admin.pro-profile');
});
Route::get('pro-categories',function(){
    return view('admin.pro-categories');
});
Route::get('pro-users',function(){
    return view('admin.pro-users');
});
Route::get('pro-articles',function(){
    return view('admin.pro-articles');
});
Route::get('pro-contact-messages',function(){
    return view('admin.pro-contact-messages');
});
Route::get('pro-slider',function(){
    return view('admin.pro-slider');
});