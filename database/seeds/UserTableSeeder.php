<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        $adminRole=Role::where('name','admin')->first();
        $userRole=Role::where('name','user')->first();

        $admin=User::create([
            'name'=>'sokvebol',
            'email'=>'sokvebol.kol@gmail.com',
            'password'=>bcrypt('123456789')
        ]);
        $user=User::create([
            'name'=>'asensio',
            'gender'=>'Male',
            'major'=>'WEB',
            'generation'=>'2018-2020',
            'plan'=>'asensio',
            'address'=>'asensio',
            'birthdate'=>'asensio',
            'email'=>'asensio.kol@gmail.com',
            'password'=>bcrypt('123456789')
        ]);

        $admin->roles()->attach($adminRole);
        $user->roles()->attach($userRole);
    }
}
